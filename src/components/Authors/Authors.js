import React, { Component } from 'react';
import Author from './Author/Author'
import faker from 'faker/locale/fa'
import AuthorHeader from './AuthorHeader/AuthorHeader'
import AuthContext from '../../containers/auth-context'

class Authors extends Component {
  constructor(props) {
    super(props)
    this.state = {
      authors: [
        { fullname: 'محمد محمدی', regDate: "1395-04-25" },
        { fullname: 'حسن حسنی', regDate: "1395-04-25" },
        { fullname: 'فردی مرکوری', regDate: "1393-05-20" },
        { fullname: 'حمید یزدان فر', regDate: "1392-04-15" }
      ],
      showAuthors: false,
      toggleCounter: 0,
      authenticated: false
    }
  }

  showHideAuthors = () => {
    console.log(this.showAuthors)
    const status = this.state.showAuthors;
    this.setState((prevState, props) => {
      return {
        showAuthors: !status,
        toggleCounter: prevState.toggleCounter + 1
      }
    })
    console.log(this.showAuthors)
  }
  removeAuthorData = (authorIndex) => {
    const authors = [...this.state.authors];
    authors.splice(authorIndex, 1);
    this.setState({ authors: authors });
  }
  changeAuthorsTwoWayDataBinding = (event, fullName) => {

    const authorIndex = this.state.authors.findIndex(author => {
      return author.fullname === fullName
    })

    const author = { ...this.state.authors[authorIndex] }
    // const author = Object.assign({}, this.state.authors[authorIndex])

    author.fullname = event.target.value;

    const authors = [...this.state.authors]
    authors[authorIndex] = author;

    this.setState({ authors: authors })
  }
  loginUser = () => {
    this.setState({ authenticated: true });
  }
  renderAuthors(show) {
    const authors = this.state.authors.map((author, index) => {
      return <Author
        fullName={author.fullname}
        regDate={author.regDate}
        avatar={faker.image.avatar()}
        click={() => this.removeAuthorData(index)}
        changed={(event) => { this.changeAuthorsTwoWayDataBinding(event, author.fullname) }}
        position={index}
        key={index}
      ></Author>
    });

    if (show) {
      return (
        <div>
          {authors}
        </div>
      )
    } else {
      return null
    }
  }
  render() {

    return (
      <div className="ui container" >
        <AuthorHeader
          clicked={this.showHideAuthors}
          authors={this.state.authors}
          login={this.loginUser} />

        <AuthContext.Provider value={this.state.authenticated}>
          {this.renderAuthors(this.state.showAuthors)}
        </AuthContext.Provider>
      </div>
    )
  }
}


export default Authors;