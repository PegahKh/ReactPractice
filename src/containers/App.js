import React, { Component, Suspense } from 'react';
import './App.css';
import Authors from '../components/Authors/Authors'
import Comments from '../components/Comments/Comments'
import withClass from '../wrappers/withClass'
import Wrapper from '../wrappers/wrapper'
import FullPost from '../components/Posts/FullPost'
import { BrowserRouter, Route, NavLink, Redirect, Switch } from 'react-router-dom'
import Counter from '../components/Counter/Counter'

const NewPost = React.lazy(() => import('../components/Posts/NewPost'))

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      showComments: true,
      auth: true
    }
  }

  render() {
    return (
      <BrowserRouter>
        <React.Fragment>
          <Wrapper>
            <header className="menu">
              <nav>
                <ul>
                  <li><NavLink to="/"
                    exact
                    activeClassName="my-active"
                    activeStyle={{
                      color: "#fff000",
                      textDecoration: "none"
                    }}
                  >صفحه اصلی</NavLink></li>
                  <li><NavLink to="/authors">نویسندگان</NavLink></li>
                  <li><NavLink to="/comments">نظرات</NavLink></li>
                  <li><NavLink to="/posts">مطالب</NavLink></li>
                  <li><NavLink to={{
                    pathname: "/new-post",
                    hash: "#submit",
                    search: "?color=true"
                  }}>مطلب جدید</NavLink></li>
                  <li><NavLink to="/counter">شمارنده</NavLink></li>
                </ul>
              </nav>
            </header>

            <Switch>
              <Route path="/authors" exact component={Authors} />
              <Route path="/comments" exact component={Comments} />
              <Route path="/posts" component={FullPost} />
              <Route path="/counter" component={Counter} />
              <Route path="/new-post"
                exact
                render={() => {
                  return (
                    <Suspense fallback={<div>در حال بارگذاری ...</div>}>
                      <NewPost />
                    </Suspense>
                  )
                }}
              />
              {/* <Redirect from="/" to="/new-post" /> */}
              <Route render={() => <h1>صفحه ای یافت نشد</h1>} />
            </Switch>

          </Wrapper>
        </React.Fragment>
      </BrowserRouter>
    );
  }
}

export default withClass(App, 'App');
