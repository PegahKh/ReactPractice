import React, { Component } from 'react'
import styles from './Post.module.css'
import axios from '../../axios';
import {Redirect} from 'react-router-dom'

class NewPost extends Component {
    constructor(props) {
        super(props)
        this.state = {
            title: '',
            description: '',
            author: 'مسعود صالحی',
            submitted: false
        }
    }
    componentDidMount(){
        console.log(this.props)
    }
    postDataToServer = () => {
        const data = {
            title: this.state.title,
            description: this.state.description,
            author: this.state.author
        }
        axios.post('/posts/', data).then(
            response => {
                console.log(response)
                this.props.history.replace('/posts')
                // this.setState({submitted: true})
            }
        )
    }
    render() {
        let redirect = null;
        if(this.state.submitted){
            redirect = <Redirect to='/posts' />
        }
        return (
            <div className="ui container">
                {redirect}
                <div className={styles.newPost}>
                    <h5>ایجاد یک مطلب جدید با متد HTTP POST</h5>
                    <div className="form-group">
                        <label>عنوان مطلب</label>
                        <input type="text" className="form-control" value={this.state.title} onChange={(event) => this.setState({ title: event.target.value })} />
                    </div>
                    <div className="form-group">
                        <label>توضیحات مطلب</label>
                        <textarea type="text" className="form-control" value={this.state.description} onChange={(event) => this.setState({ description: event.target.value })}></textarea>
                    </div>
                    <div className="form-group">
                        <label>نویسنده</label>
                        <select className="form-control" value={this.state.author} onChange={(event) => this.setState({ author: event.target.value })}>
                            <option>مسعود صالحی</option>
                            <option>مروارید قادری</option>
                        </select>
                    </div>
                    <button id="submit" className="btn btn-primary" onClick={this.postDataToServer}>ذخیره</button>
                </div>
            </div>
        )
    }
}

export default NewPost;