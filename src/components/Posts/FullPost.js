import React, { Component } from 'react';
import styles from './Post.module.css'
import axios from '../../axios';
import Post from './Post'
import { Route } from 'react-router-dom'
import SinglePost from '../Posts/SinglePost'

class FullPost extends Component {
    constructor(props) {
        super(props)
        this.state = {
            posts: [],
            error: false
        }
    }
    componentDidMount() {
        axios.get('/posts').then(
            response => {
                const posts = response.data.splice(0, 4)
                const updatedPosts = posts.map(post => {
                    return {
                        ...post,
                        author: 'مسعود صالحی'
                    }
                })
                this.setState({ posts: updatedPosts })
            }
        ).catch(
            error => {
                this.setState({ error: true })
            }
        )
    }

    selectPost = (id) => {
        this.props.history.push('/posts/' + id)
    }

    deletePost = () => {
        axios.delete('/posts/' + this.props.match.params.id).then(
            response => {
                console.log(response)
            }
        )
    }
    render() {
        let posts = null;

        if (this.state.error) {
            posts = <h4>خطایی رخ داده است لطفا مجددا تلاش کنید</h4>
        } else {
            posts = this.state.posts.map(post => {
                return (
                    <div className="col-md-3 cursor" key={post.id} >
                        <Post
                            title={post.title}
                            description={post.body}
                            author={post.author}
                            id={post.id}
                            clicked={() => this.selectPost(post.id)}
                        />
                    </div>
                )
            })
        }

        return (

            <div className="row">
                <div>
                    {posts}
                </div>
                <Route path={this.props.match.url + '/:id'} exact component={SinglePost} />
            </div>

        )
    }
}

export default FullPost;