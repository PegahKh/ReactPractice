import React from 'react'

const Comment = (props) => {
    return (
        <div className="authors">
            <img src={props.avatar} alt="" className="pull-right"/>
            <h4>{props.fullName}</h4>
            <p>{props.description}</p>
            <div>{props.regDate}</div>
        </div>
    )
}

export default Comment;