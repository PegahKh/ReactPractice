import React, { Component } from 'react';
import { connect } from 'react-redux'
import * as actionCreator from '../../store/actions/index'

class Counter extends Component {
    state = {
        counter: 0
    }

    counterChange = (action, value) => {
        switch (action) {
            case 'inc':
                this.setState((prevState) => { return { counter: prevState.counter + 1 } })
                break;
            case 'dec':
                this.setState((prevState) => { return { counter: prevState.counter - 1 } })
                break;
            case 'add':
                this.setState((prevState) => { return { counter: prevState.counter + value } })
                break;
            case 'sub':
                this.setState((prevState) => { return { counter: prevState.counter - value } })
                break;

        }
    }

    render() {
        return (
            <div>
                <div className="counter">مقدار فعلی شمارنده: {this.props.ctr}</div>
                <div className="row">
                    <div className="col-md-3"><button className="btn btn-success" onClick={this.props.onIncrementCounter}>افزایشی</button></div>
                    <div className="col-md-3"><button className="btn btn-danger" onClick={this.props.onDecrementCounter}>کاهشی</button></div>
                    <div className="col-md-3"><button className="btn btn-success" onClick={this.props.onAddCounter}>افزودن ۱۰ واحد</button></div>
                    <div className="col-md-3"><button className="btn btn-danger" onClick={this.props.onSubtractCounter}>کاهش ۱۰ واحد</button></div>
                </div>
                <hr />
                <div className="row">
                    <div className="col-xs-12">
                        <div className="text-center">
                            <button className="btn btn-primary" onClick={() => this.props.onStoreResult(this.props.ctr)}>ذخیره مقدار شمارنده</button>
                        </div>
                        <ul>
                            {this.props.storedResults.map(strResult => (
                                <li className="text-right" key={strResult.id} onClick={() => this.props.onDeleteResult(strResult.id)}>{strResult.value}</li>
                            ))}
                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        ctr: state.ctr.counter,
        storedResults: state.res.results
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onIncrementCounter: () => dispatch(actionCreator.increment()),
        onDecrementCounter: () => dispatch(actionCreator.decrement()),
        onAddCounter: () => dispatch(actionCreator.add(10)),
        onSubtractCounter: () => dispatch(actionCreator.subtract(10)),
        onStoreResult: (result) => dispatch(actionCreator.storeResult(result)),
        onDeleteResult: (id) => dispatch(actionCreator.deleteResult(id)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Counter);