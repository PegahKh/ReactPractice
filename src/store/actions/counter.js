import * as actionType from './actionType'

export const increment = () => {
    return {
        type: actionType.INC_COUNTER
    }
}
export const decrement = () => {
    return {
        type: actionType.DEC_COUNTER
    } 
}
export const add = (val) => {
    return {
        type: actionType.ADD_COUNTER,
        value: val
    }
}
export const subtract = (val) => {
    return {
        type: actionType.SUB_COUNTER,
        value: val
    }
}