import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './containers/App';
import * as serviceWorker from './serviceWorker';
import "./assets/css/bootstrap-rtl.min.css"
import "./assets/css/bootstrap.min.css"
import axios from 'axios'
import {Provider} from 'react-redux'
import {createStore, combineReducers, applyMiddleware} from 'redux'
// import reducer from './store/reducer'
import counterReducer from './store/reducers/counter'
import resultReducer from './store/reducers/result'
import thunk from 'redux-thunk'

// axios.defaults.baseURL = "https://jsonplaceholder.typicode.com"
axios.defaults.headers.common['Authentication'] = 'Auth TOKEN';
axios.defaults.headers.post['Content-Type'] = 'application/json'

axios.interceptors.request.use(request => {
    console.log(request)
    return request;
}, error =>{
    console.log(error);
    return Promise.reject(error)
})

axios.interceptors.response.use(request => {
    console.log(request)
    return request;
}, error =>{
    console.log(error);
    return Promise.reject(error)
})

const rootReducer = combineReducers({
    ctr: counterReducer,
    res: resultReducer
})

//Middleware --> میان افزارها

const police =  (store) => {
    return next => {
        return action => {
            console.log('[Middleware Dispaching', action);
            const result = next(action);
            console.log('[Middleware next state]', store.getState());
            return result
        }
    }
}

const store = createStore(rootReducer, applyMiddleware(police, thunk))

ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
