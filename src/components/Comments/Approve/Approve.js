import React from 'react'

const Approve = (props) => {
    return (
        <div className="comment">
            <button className="btn btn-sm btn-success">تایید</button>
            <button className="btn btn-sm btn-danger">رد</button>
            <div>
                {props.children}
            </div>
        </div>
    )
}

export default Approve;