import React, { Component } from 'react'
import style from './Author.css'
import withClass from '../../../wrappers/withClass'
import Wrapper from '../../../wrappers/wrapper';
import PropTypes from 'prop-types';

class Author extends Component{


    constructor(props){
        super(props);
    }

    componentDidMount(){
        if(this.props.position === 2){
            this.inputElement.focus();
        }
    }
    render(){
        return(
            <Wrapper>
                <img src={this.props.avatar} alt="" />
                <h5 className={style.redColor}>نام نویسنده: {this.props.fullName}</h5>
                <div className={style.registerDate}>تاریخ عضویت:‌ {this.props.regDate}</div>
                <p>{this.props.children}</p>
                <button className="btn btn-red" onClick={this.props.click}>حذف نویسنده</button>
                <div className="margin-top">
                    <input 
                        ref = {(inp) => {this.inputElement = inp}}
                        type="text"
                        onChange={this.props.changed} 
                        value={this.props.name}/>   
                </div>
            </Wrapper>
        )
    }
}
Author.propTypes = {
    avatar: PropTypes.string,
    fullName: PropTypes.string,
    regDate: PropTypes.string,
    click: PropTypes.func,
    changed: PropTypes.func,
}

export default withClass(Author, 'authors pull-right');