import React from 'react';
import classes from './AuthorHeader.css'
import Wrapper from '../../../wrappers/wrapper'

const authorHeader = (props) => {
    let classes = [];
    if(props.authors.length > 2){
      classes.push('author-count')
    }else{
      classes.push('purple')
    }

    return(
      <Wrapper>
        <h2>نویسندگان</h2>
        <button className="btn btn-success"
            onClick={props.clicked}
              >نمایش نویسندگان</button>
        <p className={classes.join(' ')}>تعداد نویسندگان {props.authors.length}</p>
        <button onClick={props.login}>ورود</button>
      </Wrapper>
    )

}

export default authorHeader