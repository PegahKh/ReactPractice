import React, { Component } from 'react';
import Comment from './Comment/Comment'
import Approve from './Approve/Approve'
import faker from 'faker/locale/fa'

class Comments extends Component {
  constructor(props) {
    super(props)
    this.state = {
      authors: [
        { fullname: 'محمد محمدی', regDate: "1395-04-25" },
        { fullname: 'حسن حسنی', regDate: "1395-04-25" },
        { fullname: 'فردی مرکوری', regDate: "1393-05-20" },
        { fullname: 'حمید یزدان فر', regDate: "1392-04-15" }
      ],
    }
  }
  render() {
    var comments = [];
    for (var i = 0; i < this.state.authors.length; i++) {
      comments.push(
        <Approve key={i}>
          <Comment
            fullName={faker.name.findName()}
            regDate={faker.date.recent().toDateString()}
            avatar={faker.image.avatar()}
            description={faker.lorem.paragraph()}
          />
        </Approve>
      )
    }
    return (
      <div className="ui container">
        {comments}
      </div>
    )
  }
}

export default Comments;