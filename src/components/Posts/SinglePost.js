import React, { Component } from 'react';
import styles from './Post.module.css'
import axios from '../../axios';

class SinglePost extends Component {
    constructor(props) {
        super(props)
        this.state = {
            loadedPost: null,
        }
        console.log(this.props)

    }
    componentDidMount() {
        axios.get('/posts/' + this.props.match.params.id).then(
            response => {
                this.setState({ loadedPost: response.data })
            }
        )
    }
    

    deletePost = () => {
        axios.delete('/posts/' + this.props.match.params.id).then(
            response => {
                console.log(response)
            }
        )
    }
    render() {
        let post = <h5>در حال بارگذاری مطلب</h5>
        if (this.props.match.params.id && this.state.loadedPost) {
            post = <div className={styles.full}>
                <h3>{this.state.loadedPost.title}</h3>
                <div>{this.state.loadedPost.body}</div>
                <button className="btn btn-danger" onClick={this.deletePost}>حذف</button>
            </div>
        }
        return (
            <div className="row">
                <div>
                    {post}
                </div>
            </div>
            
        )
    }
}

export default SinglePost;