import React, {Component} from 'react';
import styles from './Post.module.css'

class Post extends Component{
  constructor(props){
    super(props)
  }

  render(){
    return (
      <div className= {styles.post} onClick={this.props.clicked}>
        <h3>{this.props.title}</h3>
        <div>{this.props.description}</div>
        <br />
        <p className="small">نویسنده:‌ {this.props.author}</p>
      </div>
    )
  }
}

export default Post;